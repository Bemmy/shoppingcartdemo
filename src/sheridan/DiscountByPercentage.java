/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sheridan;

/**
 *
 * @author bmakonne
 */
public class DiscountByPercentage extends Discount {
    
    
    public DiscountByPercentage(double discountAmount, double amount ){
         this.discount = discountAmount / 100;
         this.amount = amount;
    }
    
    @Override
    public double calculateDiscount(double amount){
        
        discount *= amount;
        
        return discount;
        
    }
}
