/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sheridan;

/**
 *
 * @author bmakonne
 */
public abstract class Discount {
    
    protected double discount;
    protected double amount;
    
    public Discount(){}
    
    public abstract double calculateDiscount(double amount);
   
    public double getDiscount(){
        
        return discount;
    }
    
}
